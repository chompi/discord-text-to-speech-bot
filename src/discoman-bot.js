"use strict";

const fs = require('fs');
const __ = require('iterate-js');
const logger = require('./logger.js');
const Server = require('./structures/server.js');
const Discord = require('discord.js');
const defaults = require('./default-config.js');
const path = require('path');
const modules = __.map(fs.readdirSync('./src/modules'), mod => require(`./modules/${mod}`));

class DiscoManBot {
    
    constructor(cfg) {
        this.dir = __dirname;
        this.appDir = path.dirname(require.main.filename);
        this.client = new Discord.Client();
        this.servers = [];
        defaults(this, cfg);
        __.all(modules, mod => mod(this));
    }
    
    connect() {
        return this.client.login(this.config.discord.token);
    }
    
    disconnect() {
        return this.client.destroy();
    }
    
    listen() {
        logger.log('Listening');
        this.console.listen();
    }
    
    getServer(id) {
        if (this.servers[id] == undefined) {
            this.servers[id] = new Server(this, id);
        }
        return this.servers[id];
    }
}

module.exports = DiscoManBot;
