const fs = require('fs');
const jsonfile = require('jsonfile');
const moment = require('moment');

/**
 * Represents Users on a Guild
 */
class Users {
    
    constructor(bot, id) {
        this.bot = bot;
        this.id = id || 0;
        this.users = {};
        
        this.loadUsers();
    }
    
    getFilePath() {
        return '{0}/configs/{1}_users.json'.format(this.bot.appDir, this.id);
    }
    
    loadUsers() {
        if (fs.existsSync(this.getFilePath())) {
            var users = jsonfile.readFileSync(this.getFilePath());
            if (users)
                this.users = users;
        } else {
            this.saveUsers();
        }
    }
    
    saveUsers() {
        jsonfile.writeFileSync(this.getFilePath(), this.users);
    }
    
    getUser(userid) {
        return this.users[userid] || {};
    }
    
    setUser(userid, user) {
        this.users[userid] = user;
        this.saveUsers();
    }
}

module.exports = Users;