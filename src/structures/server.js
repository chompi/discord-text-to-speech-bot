const fs = require('fs');
const Queue = require('./queue.js');
const Users = require('./users.js');
const jsonfile = require('jsonfile');
const moment = require('moment');
const googleTTS = require('google-tts-api');
const fetch = require("node-fetch");
const request = require('superagent');

/**
 * Represents any Guild on Discord
 */
class Server {
    
    constructor(bot, id) {
        this.id = id || 0;
        this.bot = bot;
        this.queue = new Queue();
        this.users = new Users(bot, id);
        this.dispatcher = null;
        this.playing = false;
        this.textChannel = null;
        this.voiceChannel = null;
        this.voiceConnection = null;
        this.lastUser = null;
        this.lastTimestamp = 0;
        
        this.config = {
            tts: {
                lang: this.bot.config.tts.lang,
                speed: this.bot.config.tts.speed,
            },
            command: {
                symbol: this.bot.config.command.symbol,
                bindToChannel: this.bot.config.command.bindToChannel
            },
            volume: this.bot.config.stream.volume
            
        };
        this.loadConfig();
    }
    
    getConfigPath() {
        return '{0}/configs/{1}.json'.format(this.bot.appDir, this.id);
    }
    
    getConfig() {
        return this.config;
    }
    
    loadConfig() {
        if (fs.existsSync(this.getConfigPath())) {
            var config = jsonfile.readFileSync(this.getConfigPath());
            if (config)
                this.config = config;
        } else {
            this.saveConfig();
        }
    }
    
    saveConfig() {
        jsonfile.writeFileSync(this.getConfigPath(), this.config);
    }
    
    setNickname(userid, nickname) {
        var user = this.users.getUser(userid);
        
        user.nickname = nickname;
        this.users.setUser(userid, user);
    }
    
    ignoreUser(userid) {
        var user = this.users.getUser(userid);
        
        user.ignore = true;
        this.users.setUser(userid, user);
    }
    
    noticeUser(userid) {
        var user = this.users.getUser(userid);
        
        user.ignore = false;
        this.users.setUser(userid, user);
    }
    
    getVolume() {
        return this.config.volume;
    }
    
    setVolume(volume) {
        this.config.volume = volume;
        this.saveConfig();
    }
    
    getLang() {
        return this.config.tts.lang;
    }
    
    setLang(lang) {
        this.config.tts.lang = lang;
        this.saveConfig();
    }
    
    getSpeed() {
        return this.config.tts.speed;
    }
    
    setSpeed(speed) {
        this.config.tts.speed = speed;
        this.saveConfig();
    }
    
    getCommandSymbol() {
        return this.config.command.symbol || this.bot.config.command.symbol;
    }
    
    setTextChannel(textChannel) {
        this.textChannel = textChannel;
    }
    
    getTextChannel() {
        return this.textChannel;
    }
    
    getVoiceConnection() {
        return (this.textChannel && this.textChannel.guild ? this.textChannel.guild.voiceConnection : null);
    }
    
    queueMessage(msg) {
        var user = this.users.getUser(msg.author.id);
        
        if (this.bot.config.tts.ignoreAll && (user.ignore == undefined || user.ignore == true))
            return;
        
        this.queue.enqueue(msg);
        
        if (!this.playing) {
            this.playing = true;
            var message = this.queue.dequeue();
            this.process(message);
        }
    }
    
    process(msg) {
        var $server = this;
        var text, nickname;
        var user = this.users.getUser(msg.author.id);
        var timeDifference = (this.lastTimestamp > 0 ? Date.now() - this.lastTimestamp : 0);
        
        if (user.nickname != undefined) {
            nickname = user.nickname;
        } else {
            nickname = msg.author.username;
        }
        
        if (timeDifference < (120 * 1000) && this.lastUser && this.lastUser == msg.author.id) {
            text = `${msg.content}`;
        } else {
            text = `${nickname} said ${msg.content}`;
        }
        
        this.lastUser = msg.author.id;
        
        // Filter out custom emojis
        var emojiRegex = /\<\:([\w]+)\:[0-9]+\>/gi;
        text = text.replace(emojiRegex, ':$1:');
        
        // Filter mentions
        var mentionRegex = /\<\@\!([0-9]+)\>/gi;
        text = text.replace(mentionRegex, function(str, p1, p2) {
            var nick;
            var u1 = $server.bot.client.users.get(p1);
            var u2 = $server.users.getUser(u1.id);
            if (u1) nick = u1.username;
            if (u2.nickname != undefined) nick = u2.nickname;
            return nick;
        });
        
        // Filter out long ass numbers
        var numbersRegex = /([\d]{7,})/gi;
        text = text.replace(numbersRegex, 'long number');
        
        var jokes = {
            "287140364677283852": ["A man's not hot.", "Disco Disco wah wah", "Shush this is my creator speaking."], // Savage
            "234352097875722241": ["And by the way i'm not a bird.", "You slaves.", "Im not a bully."], // Emus
            "196628785318068224": ["Also im very shy... Not.", "I love being a teenager.", "Japanese chicks are so tight."], // IHaz
            "78509560863666176": ["Cluck cluck.", "I was an egg once.", "The chicken farmer died under mysterious circumstances."], // Chicken
            "223456268692488193": ["Im not always a bitch, just kidding.", "Let's do relics."], // Eersa
            "169197725315760128": ["My hair is so lovely.", "Not like this.", "Shit im Baka."], // Tali
            "226469111608115200": ["Ducks are amazing.", "Pepsi is amazing.", "I love pepsi.", "I secretly eat meat."], // Duck
        };
        
        var jokeChance = 0.05;
        
        if (Math.random() < jokeChance && jokes[msg.author.id] != undefined) {
            var userJokes = jokes[msg.author.id];
            text += ". " + userJokes[Math.floor(Math.random() * Math.floor(userJokes.length))];
        }
        
        googleTTS(text, this.getLang(), this.getSpeed())   // speed normal = 1 (default), slow = 0.24
        .then(function(url) {
            var stream = fs.createWriteStream('voice.wav');
            stream.on("finish", () => {
                $server.fileAvailable(stream.path);
            });
            stream.on("error", (err) => {
                $server.processNext();
                console.log(err);
            });
            
            var req = request
                .get(url)
                .timeout({
                    response: 1000,
                    deadline: 5000,
                })
                .pipe(stream);
        })
        .catch(function(err) {
            $server.processNext();
            console.error(err.stack);
        });
        
        this.lastTimestamp = Date.now();
    }
        
    fileAvailable(filePath) {
        var $server = this;
        var voiceConnection = this.getVoiceConnection();
        if (voiceConnection != undefined) {
            const options = { seek: 0, passes: this.bot.config.stream.passes, volume: this.getVolume() };
            this.dispatcher = voiceConnection.playFile(filePath, options);
            this.dispatcher.on('start', () => {
                voiceConnection.player.streamingData.pausedTime = 0;
            });
            this.dispatcher.on('end', () => {
                $server.dispatcher = null;
                $server.processNext();
            });
            this.dispatcher.on('error', (err) => {
                $server.processNext();
                console.error(err);
            });
        }
    }
    
    processNext() {
        if (this.queue.count > 0) {
            var message = this.queue.dequeue();
            this.process(message);
        } else {
            this.playing = false;
        }
    }
}

module.exports = Server;
