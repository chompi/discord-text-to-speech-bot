module.exports = [
    '{0}ping: "Check if the bot is online"',
    '{0}join: "Join your voice channel"',
    '{0}leave: "Leave current voice channel"',
    '{0}volume [0-100 or nothing]: "Sets volume to a number between 0-100" Ex: {0}volume 43',
    '{0}speed [1-100]: "Sets the bot speech speed"',
    '{0}nickname [your nickname]: "Sets your nickname"',
    '{0}ignore: "Ignores the user writings"',
    '{0}notice: "Notices the user writings"',
    '{0}config help: "Config help menu" (Requires Administrator)'
].join('\n');
