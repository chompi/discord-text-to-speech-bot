const __ = require('iterate-js');
const logger = require('../logger.js');

module.exports = function(bot) {

    var parseMsg = (msg) => {
        msg.meta = msg.content.split(' ');
        var x = msg.meta.slice();
        msg.cmd = x.shift().replace(msg.server.getCommandSymbol(), '').toLowerCase();
        msg.details = x.join(' ');
        return msg;
    };

    __.all({
        message: msg => {
            // Cannot operate outside of a guild
            if (!msg.guild)
                return;
            
            // Messages by users
            if (msg.author.id != bot.client.user.id) {
                // Get the server object
                msg.server = bot.getServer(msg.guild.id);

                // Check if the commands are bound to a channel
                if (msg.server.config.command.bindToChannel && msg.channel.id != msg.server.config.command.bindToChannel)
                    return;

                var commandSymbol = msg.server.getCommandSymbol();
                var hasCommand = (content) => content.substring(0, commandSymbol.length) == commandSymbol;

                if (msg.content && hasCommand(msg.content)) {
                    // Set the text channel on the server object
                    msg.server.setTextChannel(msg.channel);

                    if (bot.config.discord.log) {
                        logger.log('{0}{1}{2} : {3}'.format(
                            msg.guild ? '{0} '.format(msg.guild.name) : '', 
                            msg.channel.name ? '#{0} @ '.format(msg.channel.name) : 'PM @ ', 
                            msg.author.username, 
                            msg.content
                        ));
                    }

                    try {
                        var data = parseMsg(msg),
                            cmd = bot.commands[data.cmd];
                        if (__.is.function(cmd))
                            cmd(data);
                    } catch(e) {
                        logger.error(e);
                    }
                } else if (msg.content) {
                    // process simple text
                    if (msg.server.getVoiceConnection()) {
                        msg.server.queueMessage(msg);
                    }
                }
            } else {
                // Clean bot messages
                try {
                    bot.manager.cleanChannel(msg.channel);
                } catch(e) {
                    logger.error(e);
                }
            }
        },

        ready: () => {
            if (bot.online)
                logger.log('Reconnected.');
            else
                logger.log('Text-To-Speech Bot Online.');
            bot.online = true;
        },

        reconnecting: () => {
            logger.log('Reconnecting...');
        },

        disconnect: () => {
            bot.online = false;
            logger.log('Disconnected.');
        },

        error: error => {
            logger.error(error);
        },

        guildMemberUpdate: (old, member) => {
            if (member.user.username == bot.client.user.username && member.mute) {
                member.setMute(false);
                logger.log('Text-To-Speech Bot muted....unmuteing');
            }
        }
        
    }, (func, name) => { bot.client.on(name, func); });
};
