
const __ = require('iterate-js');
const moment = require('moment');
const logger = require('../logger.js');

module.exports = function(bot) {
    bot.manager = {

        cleaning: {},

        sequentialDelete: function(msgs, cb) {
            var msg = msgs.shift();
            if (msg) {
                msg.delete()
                    .then(() => { bot.manager.sequentialDelete(msgs, cb); })
                    .catch(error => logger.error(error));
            } else if(cb) {
                cb();
            }
        },

        cleanChannel: function(channel) {
            var server = bot.getServer(channel.guild.id);
            var cfg = server.config.messages || bot.config.messages;
            
            if (channel && channel.type == 'text' && !bot.manager.cleaning[channel.name]) {
                bot.manager.cleaning[channel.name] = true;
                
                channel.fetchMessages({ limit: 100 }).then(() => {
                    var msgs = __.sort(channel.messages.array(), { key: v => v.createdTimestamp, dir: 'desc' });

                    // Filter out non bot messages
                    __.all(msgs.slice(), (msg, idx) => {
                        if (msg.author.id != bot.client.user.id) {
                            var i = msgs.indexOf(msg);
                            if (i > -1) msgs.splice(i, 1);
                        }
                    });

                    // Filter out the limit of messages
                    __.all(msgs.slice(), (msg, idx) => {
                        if (idx < cfg.limitPerChannel) {
                            var i = msgs.indexOf(msg);
                            if (i > -1)
                                msgs.splice(i, 1);
                        }
                    });

                    if (msgs.length > 0) {
                        bot.manager.sequentialDelete(msgs, () => {
                            bot.manager.cleaning[channel.name] = false;
                            bot.manager.cleanChannel(channel);
                        });
                    } else
                        bot.manager.cleaning[channel.name] = false;
                })
                .catch(error => {
                    bot.manager.cleaning[channel.name] = false;
                });
            }
        }
    };
};
