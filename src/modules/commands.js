const __            = require('iterate-js');
const moment        = require('moment');
const logger        = require('../logger.js');
const helpText      = require('../helptext.js');
const helpTextCfg   = require('../helptext-config.js');
const Server        = require('../structures/server.js');
const util          = require('../utils');

module.exports = function(bot) {
    
    bot.commands = {

        help: msg => {
            msg.channel.send('Help Menu :question:\n```' + helpText.format(msg.server.getCommandSymbol()) + '```', { split: true });
        },

        ping: msg => {
            msg.channel.send(`:ping_pong: Pong!`);
        },

        join: msg => {
            return new Promise((resolve, reject) => {
                var voicechannel = msg.member.voiceChannel;
                if (voicechannel && voicechannel.type == 'voice') {
                    // Join the voice channel
                    voicechannel.join()
                        .then(connection => {
                            resolve(connection);
                            msg.channel.send(`:speaking_head: Joined channel: ${voicechannel.name}`);
                            connection.on('error', (error) => {
                                logger.error(error);
                            });
                        }).catch(err => reject(err));
                } else
                    return msg.channel.send("I couldn't connect to your voice channel.");
            });
        },

        summon: msg => {
            return bot.commands.join(msg);
        },
        
        leave: msg => {
            if (msg.guild.voiceConnection) {
                msg.channel.send(`:mute: Disconnecting from channel: ${msg.guild.voiceConnection.channel.name}`);
                msg.guild.voiceConnection.disconnect();
            }
        },
        
        volume: msg => {
            var volume = msg.details.trim();
            if (volume != '') {
                volume = __.math.between(parseInt(volume), 0, 100);
                volume = (volume / 100);
                msg.server.setVolume(volume);
                msg.channel.send(`:speaker: Volume set to ${volume * 100}%`);
            } else
                msg.channel.send(`:speaker: Volume set to ${msg.server.getVolume() * 100}%`);
        },
        
        bind: msg => {
            msg.server.config.command.bindToChannel = msg.channel.id;
            msg.server.saveConfig();
            msg.channel.send(`Commands are now bound to channel "${msg.channel.name}".`);
        },
        
        unbind: msg => {
            msg.server.config.command.bindToChannel = null;
            msg.server.saveConfig();
            msg.channel.send(`Commands are no longer bound to a channel.`);
        },
        
        language: msg => {
            var lang = msg.details.trim();
            if (lang != '') {
                msg.server.setLang(lang);
                msg.channel.send(`:speaker: Language set to ${lang}.`);
            }
        },
        
        lang: msg => {
             return bot.commands.language(msg); 
        },
        
        speed: msg => {
            var speed = msg.details.trim();
            if (speed != '') {
                speed = __.math.between(parseInt(speed), 1, 100);
                speed = (speed / 100);
                msg.server.setSpeed(speed);
                msg.channel.send(`:speaker: Speed set to ${speed * 100}%`);
            } else
                msg.channel.send(`:speaker: Speed set to ${msg.server.getSpeed() * 100}%`);
        },
        
        nickname: msg => {
            var nickname = msg.details.trim();
            if (nickname != '' && msg.author) {
                if (nickname.length > 3 && nickname.length < 20) {
                    msg.server.setNickname(msg.author.id, nickname);
                    msg.channel.send(`Your nickname was set to ${nickname}.`);
                } else {
                    msg.channel.send(`Nickname must be between 3 and 20 characters.`);
                }
            } else
                msg.channel.send(`Please enter a nickname.`);
        },
        
        ignore: msg => {
            if (msg.author) {
                msg.server.ignoreUser(msg.author.id);
                msg.channel.send(`Ignoring ${msg.author.username}.`);
            }
        },
        
        notice: msg => {
            if (msg.author) {
                msg.server.noticeUser(msg.author.id);
                msg.channel.send(`Noticing ${msg.author.username}.`);
            }
        },
        
        config: msg => {
            var group = msg.details.substr(0, msg.details.indexOf(' '));
            var parameters = msg.details.substr(msg.details.indexOf(' ') + 1).split(' ');
            var setting = parameters[0].toLowerCase();
            var value = parameters[1];
            
            // Check for admin permission
            if (!msg.member || !msg.member.hasPermission('ADMINISTRATOR')) {
                msg.channel.send(`You don't have permissions to do that.`);
                return;
            }
            
            __.switch(group, {
                command: () => {
                    __.switch(setting, {
                        symbol: () => {
                            if (value != undefined) {
                                msg.server.config.command.symbol = value;
                                msg.server.saveConfig();
                                msg.channel.send(`Command symbol has been set to "${value}".`);
                            }
                        }
                    });
                }
            });
            
            // Handle help
            if (msg.details.trim().toLowerCase() == 'help') {
                msg.channel.send('Config Help Menu :question:\n```' + helpTextCfg.format(msg.server.getCommandSymbol()) + '```', { split: true });
            }
        }
    };
};