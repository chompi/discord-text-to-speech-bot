const __ = require('iterate-js');

module.exports = function(bot, config) {
    bot.config = new __.lib.Config({
        auto: new __.lib.Config({
            reconnect: true
        }),
        tts: new __.lib.Config({
            lang: 'en',
            speed: 1,
            ignoreAll: false // Include all users by default
        }),
        command: new __.lib.Config({
            symbol: '!',
            bindToChannel: null
        }),
        discord: new __.lib.Config({
            token: '<BOT-TOKEN>',
            log: true
        }),
        stream: new __.lib.Config({
            passes: 2, //can be increased to reduce packetloss at the expense of upload bandwidth, 4-5 should be lossless at the expense of 4-5x upload
            volume: 0.1
        })
    });
    bot.config.update(config);
};